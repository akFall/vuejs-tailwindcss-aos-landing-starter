import Vue from 'vue'
import Landing from './views/Landing.vue'

import "@fortawesome/fontawesome-free/css/all.min.css";
import "aos/dist/aos.css";
import "./assets/styles/index.css"
import AOS from 'aos';

Vue.config.productionTip = false

const routes = {
  '/landing': Landing
}

new Vue({
  created() {
    AOS.init({
      duration: 1500,
      once: false
    });
  },
  data: {
    currentRoute: window.location.pathname
  },
  computed: {
    ViewComponent () {
      return routes[this.currentRoute] || Landing
    }
  },
  render (h) { return h(this.ViewComponent) },
}).$mount('#app')
